/* global oComponent_ManagerReport  : true */
var oComponent_ManagerReport;
sap.ui.define(
    ["sap/ui/core/UIComponent", "sap/ui/Device", "ZMM_MANAGER_REPORT_PO/model/models"],
    function (UIComponent, Device, models) {
        "use strict";

        return UIComponent.extend("ZMM_MANAGER_REPORT_PO.Component", {
            metadata: {
                manifest: "json"
            },

            /**
             * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
             * @public
             * @override
             */
            init: function () {
                oComponent_ManagerReport = this;
                // call the base component's init function
                UIComponent.prototype.init.apply(this, arguments);

                // enable routing
                this.getRouter().initialize();
                sap.ui.getCore().getConfiguration().setRTL(true);
                //Set HE language
                sap.ui.getCore().getConfiguration().setLanguage("iw_IL");
                this.setModel(models.createJSONModel(), "JSON");
                // set the device model
                this.setModel(models.createDeviceModel(), "device");
            }
        });
    }
);
