/* global oComponent_ManagerReport  : true */
sap.ui.define(
    [
        "sap/ui/core/mvc/Controller",
        "sap/ui/core/routing/History",
        "sap/ui/core/UIComponent",
        "sap/ui/model/Filter",
        "sap/ui/model/FilterOperator",
        "ZMM_MANAGER_REPORT_PO/model/formatter",
        "ZMM_MANAGER_REPORT_PO/model/models",
        "sap/m/MessageBox"
    ],
    function (Controller, History, UIComponent, Filter, FilterOperator, formatter, models, MessageBox) {
        "use strict";

        return Controller.extend("ZMM_MANAGER_REPORT_PO.controller.BaseController", {
            formatter: formatter,

            /**
             * Convenience method for getting the view model by name in every controller of the application.
             * @public
             * @param {string} sName the model name
             * @returns {sap.ui.model.Model} the model instance
             */
            getModel: function (sName) {
                return this.getView().getModel(sName);
            },

            /**
             * Convenience method for setting the view model in every controller of the application.
             * @public
             * @param {sap.ui.model.Model} oModel the model instance
             * @param {string} sName the model name
             * @returns {sap.ui.mvc.View} the view instance
             */
            setModel: function (oModel, sName) {
                return this.getView().setModel(oModel, sName);
            },

            /**
             * Convenience method for getting the resource bundle.
             * @public
             * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
             */
            getResourceBundle: function () {
                return this.getOwnerComponent().getModel("i18n").getResourceBundle();
            },

            /**
             * Method for navigation to specific view
             * @public
             * @param {string} psTarget Parameter containing the string for the target navigation
             * @param {Object.<string, string>} pmParameters? Parameters for navigation
             * @param {boolean} pbReplace? Defines if the hash should be replaced (no browser history entry) or set (browser history entry)
             */
            navTo: function (psTarget, pmParameters, pbReplace) {
                this.getRouter().navTo(psTarget, pmParameters, pbReplace);
            },

            getRouter: function () {
                return UIComponent.getRouterFor(this);
            },
            onOpenDialog: function (oEvent, sDialogName) {
                if (!this[sDialogName]) {
                    this[sDialogName] = sap.ui.xmlfragment("ZMM_MANAGER_REPORT_PO.view.fragments." + sDialogName + 'Dialog', this);
                    this.getView().addDependent(this[sDialogName]);
                }
                this[sDialogName].open();
            },
            onCloseDialog: function (sDialogName) {
                this[sDialogName].close();
            },
            onValueHelpSearch: function (oEvent) {
                debugger;
                var sValue = oEvent.getParameter("value");
                var oFilter = [];
                oFilter.push(new Filter("Name", FilterOperator.Contains, sValue));
                var oBinding = oEvent.getParameter("itemsBinding");
                oBinding.filter(oFilter);
            },
            onValueHelpClose: function (oEvent) {
                debugger;
                var oSelectedItem = oEvent.getParameter("selectedItem");
                oEvent.getSource().getBinding("items").filter([]);

                if (!oSelectedItem) {
                    return;
                }
                var skostl = oSelectedItem.getProperty("description");
                oComponent_ManagerReport.getModel("JSON").setProperty("/Filters/CostcenterSh", skostl);
                oComponent_ManagerReport.getModel("JSON").setProperty("/Filters/CostcenterText", oSelectedItem.getTitle());
            },
            onNavBack: function () {
                var sPreviousHash = History.getInstance().getPreviousHash();

                if (sPreviousHash !== undefined) {
                    window.history.back();
                } else {
                    this.getRouter().navTo("appHome", {}, true /*no history*/);
                }
            },
            openOutput: function (sOrderNum) {
                models.LoadOutput(sOrderNum).then(function (data) {
                    var byteCharacters = atob(data.EvOutput);
                    var byteNumbers = new Array(byteCharacters.length);
                    for (var i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }
                    var byteArray = new Uint8Array(byteNumbers);
                    if (window.navigator.msSaveOrOpenBlob) {
                        var blob = new Blob([byteArray], { type: 'application/pdf' });
                        window.navigator.msSaveOrOpenBlob(blob, "Draft" + ".pdf");
                    }
                    else {
                        var file = new Blob([byteArray], { type: 'application/pdf;base64' });
                        var fileURL = URL.createObjectURL(file);
                        window.open(fileURL);
                    }
                }).catch(function (error) {
                    try {
                        MessageBox.error(JSON.parse(error.responseText).error.message.value);
                    } catch (e) {
                        MessageBox.error(JSON.stringify(error));
                    }
                });

            },
            goToApp: function (oEvent, orderNum) {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = "zmm_create_po-create";
                var actionURL = "create&/Items/" + orderNum;
                // var oParams = {
                //     OrderNumber: orderNum
                // };
                // this.setAppState(oCrossAppNavigator);
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done((aResponses) => {
                        if (aResponses[semamticActionObj].supported === true) {
                            var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                                target: {
                                    semanticObject: 'zmm_create_po',
                                    action: actionURL
                                }
                            })) || "";
                            hash= hash.split("?")[0];
                            sap.m.URLHelper.redirect(hash, true);
                        }

                    })
                    .fail(function () { });
            },
            // goToApp: function (oEvent, orderNum) {
            //     var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
            //     var semamticActionObj = "zmm_create_po-create";
            //     var actionURL = "create&/Items/" + orderNum;
            //     // var oParams = {
            //     //     OrderNumber: orderNum
            //     // };
            //     // this.setAppState(oCrossAppNavigator);
            //     oCrossAppNavigator.isIntentSupported([semamticActionObj])
            //         .done(function (aResponses) {
            //             if (aResponses[semamticActionObj].supported === true) {
            //                 var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
            //                     target: {
            //                         semanticObject: "zmm_create_po",
            //                         action: actionURL
            //                     },
            //                     // params: oParams
            //                 })) || "";
            //                 oCrossAppNavigator.toExternal({
            //                     target: {
            //                         shellHash: hash
            //                     }
            //                 });
            //             }

            //         })
            //         .fail(function () { });
            // },
            GoToME23N: function (event, sOrder) {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = "MMPURPAMEPO-display";
                var oParams = {
                    PurchaseOrder: sOrder
                };
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done((aResponses) => {
                        if (aResponses[semamticActionObj].supported === true) {
                            var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                                target: {
                                    semanticObject: "MMPURPAMEPO",
                                    action: "display"
                                },
                                params: oParams
                            })) || "";
                            oCrossAppNavigator.toExternal({
                                target: {
                                    shellHash: hash
                                }
                            });
                        }

                    })
                    .fail(function () { });
            },
            GoToMigoGr: function (event, order) {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = "MIGO_GR-postGoodsReceipt";
                var oParams = {
                    PurchaseOrder: order
                };
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done((aResponses) => {
                        if (aResponses[semamticActionObj].supported === true) {
                            var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                                target: {
                                    semanticObject: "MIGO_GR",
                                    action: "postGoodsReceipt"
                                },
                                params: oParams
                            })) || "";
                            oCrossAppNavigator.toExternal({
                                target: {
                                    shellHash: hash
                                }
                            });
                        }

                    })
                    .fail(function () { });
            }
        });
    }
);
