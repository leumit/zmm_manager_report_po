/* global oComponent_ManagerReport  : true */
sap.ui.define(["ZMM_MANAGER_REPORT_PO/controller/BaseController","sap/ui/model/Filter","sap/ui/export/Spreadsheet","ZMM_MANAGER_REPORT_PO/model/models","ZMM_MANAGER_REPORT_PO/model/formatter"], function (Controller,Filter,Spreadsheet,models,formatter) {
    "use strict";

    return Controller.extend("ZMM_MANAGER_REPORT_PO.controller.MainView", {
        /**
         * @override
         */
		 formatter:formatter,
        onInit: function() {
            oComponent_ManagerReport._MainController = this;
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                oRouter.getRoute('RouteMainView').attachPatternMatched(this.onMainPress, this);
				oRouter.getRoute('Default').attachPatternMatched(this.onMainPress, this);
				
        
        },
		onMainPress: function(oEvent){
			var sType  = '',
				sMatnr ='',
				sDefectCode ='';
			try{
				sType = oComponent_ManagerReport.getComponentData().startupParameters["type"][0];
			}catch (e) {}
			try{
				sMatnr = oComponent_ManagerReport.getComponentData().startupParameters["Matnr"][0];

			}catch (e) {}
			
			oComponent_ManagerReport.getModel("JSON").setProperty("/AppType",sType);
			oComponent_ManagerReport.getModel("JSON").setProperty("/Matnr", sMatnr);
			if(sType === 'Defect'){
				sDefectCode = '1';
				oComponent_ManagerReport.getModel("JSON").setProperty("/Filters/StatusSH",'1');
			}
			oComponent_ManagerReport.getModel("JSON").setProperty("/DefectCode",sDefectCode);
            this.getData(sMatnr,sDefectCode);
		},
		getData: function(Matnr , sType){
			models.loadReportData(Matnr ,sType).then(function(data) {
				var aOrders =[];
				for(var i = 0; i<data.results.length; i++){
					aOrders.push({Num: data.results[i].Ordernumber});
					data.results[i].OrdervalueFloat = parseFloat(data.results[i].Ordervalue);
				}
				aOrders = oComponent_ManagerReport._MainController.removeDuplicates(aOrders, "Num");		
				oComponent_ManagerReport.getModel("JSON").setProperty("/OrdersNumbers" , aOrders);
				oComponent_ManagerReport.getModel("JSON").setProperty("/items" , data.results);
				oComponent_ManagerReport.getModel("JSON").setProperty("/count" , data.results.length);
			}).catch(function (error) {
			});
		},
        _filter: function() {
			var oFilter = null;
			if (this._oGlobalFilter && this._oPriceFilter) {
				oFilter = new sap.ui.model.Filter([this._oGlobalFilter, this._oPriceFilter], true);
			} else if (this._oGlobalFilter) {
				oFilter = this._oGlobalFilter;
			} else if (this._oPriceFilter) {
				oFilter = this._oPriceFilter;
			}

			oComponent_ManagerReport._MainController.byId("dataTable").getBinding("rows").filter(oFilter, "Application");
			oComponent_ManagerReport.getModel("JSON").setProperty("/count", oComponent_ManagerReport._MainController.byId("dataTable").getBinding("rows").getLength());
		},

		filterGlobally: function(oEvent) {
			this._oGlobalFilter = null;
			var oFilters = [];
			var model = oComponent_ManagerReport.getModel("JSON"),
				fData = model.getProperty("/Filters");
			if (fData.DocDateFrom || fData.DocDateTo) {
				if (!fData.DocDateFrom) { // only to
					oFilters.push(new sap.ui.model.Filter("Createdat", "LE", fData.DocDateTo));
				} else if (!fData.DocDateTo) { // only from
					oFilters.push(new sap.ui.model.Filter("Createdat", "GE", fData.DocDateFrom));
				} else { // from && to
					oFilters.push(new sap.ui.model.Filter("Createdat", "BT", fData.DocDateFrom, fData.DocDateTo));
				}
			}
			if (fData.OrderSH) {
                    oFilters.push(new sap.ui.model.Filter("Ordernumber", "EQ", fData.OrderSH));
            }
			if (fData.CostcenterSh) {
                    oFilters.push(new sap.ui.model.Filter("Costcenter", "EQ", fData.CostcenterSh));
            }
			if (fData.VendorSH) {
				oFilters.push(new sap.ui.model.Filter("Vendor", "EQ", fData.VendorSH));
			}
			if (fData.StatusSH) {
			oFilters.push(new sap.ui.model.Filter("Orderstatus", "EQ", fData.StatusSH));
			}

			if (oFilters.length !== 0) {
				this._oGlobalFilter = new Filter(oFilters, true);
			}

			this._filter();
		},

		clearAllFilters: function(oEvent) {
			var oTable = oComponent_ManagerReport._MainController.byId("dataTable");
			this._oGlobalFilter = null;
			this._oPriceFilter = null;
			this._filter();
			var aColumns = oTable.getColumns();
			for (var i = 0; i < aColumns.length; i++) {
				oTable.filter(aColumns[i], null);
				oTable.sort(aColumns[i], null);
			}
			oComponent_ManagerReport.getModel("JSON").setProperty("/Filters", []);
		},
		getOutput: function(םEvent , sOrderNum){
			
		},
		createColumnConfig: function() {
			var aCols = oComponent_ManagerReport._MainController.byId("dataTable").getColumns();
			var aColumns = [];
			for (var i = 0; i < aCols.length; i++) {
				if (oComponent_ManagerReport._MainController.byId("dataTable").getColumns()[i].getVisible()) {
					aColumns.push({
						label: aCols[i].getLabel().getProperty("text") ||" ",
						property: aCols[i].getFilterProperty()|| " ",
						width: '15rem'
					});
				}
			}
			return aColumns;

		},
		onExport: function() {
			var aCols, aItems, oSettings, oSheet;
			aCols = this.createColumnConfig();
			aItems = oComponent_ManagerReport.getModel("JSON");
			var aIndices = oComponent_ManagerReport._MainController.byId("dataTable").getBinding("rows").aIndices;
			var aSelectedModel = [];
			for (var i = 0; i<aIndices.length; i++){
				aSelectedModel.push(aItems.getProperty("/items/" + aIndices[i] ));
			}
			// var aSelectedModel = oComponent_ManagerReport._MainController.byId("dataTable").getBinding("rows").oList;
			oSettings = {
				workbook: {
					columns: aCols
				},
				dataSource: aSelectedModel,
				fileName: "הזמנות רכש"
			};

			oSheet = new Spreadsheet(oSettings);
			oSheet.build()
				.then(function() {});

		},
		removeDuplicates: function(array, key) {
			let lookup = new Set();
			return array.filter(obj => !lookup.has(obj[key]) && lookup.add(obj[key]));
		}
    });
});
