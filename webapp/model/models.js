sap.ui.define(["sap/ui/model/json/JSONModel", "sap/ui/Device","sap/ui/model/Filter"], function (JSONModel, Device,Filter) {
    "use strict";

    return {
        createDeviceModel: function () {
            var oModel = new JSONModel(Device);
            oModel.setDefaultBindingMode("OneWay");
            return oModel;
        },
        loadReportData: function(Matnr, sType){
            var aFilters = [];
			aFilters.push(new Filter("IvOrderStatus", sap.ui.model.FilterOperator.EQ, sType));
            aFilters.push(new Filter("IvMatnr", sap.ui.model.FilterOperator.EQ, Matnr));
			return new Promise(function(resolve, reject) {
				oComponent_ManagerReport.getModel("ODATA").read("/OrderReportSet", {
                    filters: aFilters,
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
		},
		LoadOutput: function(sOrderNum){
			var sKey = oComponent_ManagerReport.getModel("ODATA").createKey("/OrderPdfOutputSet", {
				IvPoNumber : sOrderNum
			})
            return new Promise(function(resolve, reject) {
				oComponent_ManagerReport.getModel("ODATA").read(sKey, {
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
        },
        
        createJSONModel: function () {
			var oModel = new sap.ui.model.json.JSONModel({
                Filters: []
			});
			oModel.setSizeLimit(1000);
			return oModel;
		}
    };
});
