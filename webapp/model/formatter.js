sap.ui.define([], function () {
    "use strict";
    return {
        addComma: function(value) {
			if (value) {
				value = value.toString();
				return value.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}
			return value;
		},
        getOrderStatus: function(status){
            if(status === '1'){
                return 'None';
            }
            else if(status === '2'){
                return 'Warning';
                
            }
            else if(status === '3'){
                return 'Indication03';
                
            }
            else if(status === '4'){
                return 'Success';
                
            }
            else if(status === '5'){
                return 'Information';
                
            }
            else if(status === '10'){
                return 'Error';
            }
            else return 'None'
        }
    };
});
